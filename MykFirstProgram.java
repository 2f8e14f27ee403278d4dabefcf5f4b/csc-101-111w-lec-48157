// This is my first program.
// It will print:
// "Hello World!"
// "My name is Mykael Alexander."
// "My major is Computer Science."

public class MykFirstProgram
{
  public static void main(String[] args)
  {
    System.out.println( "Hello World!");
    System.out.println( "My name is Mykael Alexander.");
    System.out.println( "My major is Computer Science.");
  }
}
